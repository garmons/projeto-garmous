<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/helpers/Config.php';
include_once CABECALHO;
?>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="preconnect" href="https://fonts.googleapis.com">

<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Great+Vibes&family=Parisienne&display=swap" rel="stylesheet">
<link href="/assets/css/mains.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/assets/js/main.js"></script>

<div class=" fundoBackground  textoBranco">
    <div class="container">
        <div class=" row pb-5">
            <div class="col-md-8 ">
                <h1 class="fonteTitulo pt-5">Solicite um Pedido!</h1>
                <p>
                    Preencha a ficha de pedidos, que ficará disponivel para os nossos
                    profissionais <br> se candidatarem para sua vaga e aceitarem a oferta.
                </p>
                <a href="#cadastro" class="btn btn-danger ">Faça uma solicitação</a>
            </div>
            <div class="col-md-4">
                <img id="imgBorda" src="/assets/img/mesapestisco.png" class="imgnone" width="350px">
            </div>
        </div>
    </div>
</div>

<div class="fundoBackground3">
    <div class="container">
        <h3 class="fonteSubTitulo textoBranco">Filtros</h3>
        <div class="row  ">
            <div class="col-md d-flex justify-content-center">
                <select class="btn btn-light btn-lg m-3 ">
                    <option selected>Categorias</option>
                    <option>1</option>
                </select>
            </div>
            <div class="col-md d-flex justify-content-center">
                <select class="btn btn-light btn-lg m-3">
                    <option selected>Idade</option>
                    <option>1</option>
                </select>
            </div>
            <div class="col-md d-flex justify-content-center">
                <select class="btn btn-light btn-lg m-3">
                    <option selected>Genero</option>
                    <option>1</option>
                </select>
            </div>
            <div class="col-md d-flex justify-content-center">
                <select class="btn btn-light btn-lg m-3">
                    <option selected>Avaliação</option>
                    <option>1</option>
                </select>
            </div>
            <div class="col-md d-flex justify-content-center">
                <select class="btn btn-light btn-lg m-3 ">
                    <option selected>Cidade</option>
                    <option>1</option>
                </select>
            </div>
        </div>
    </div>
</div>

<h1 class="fonteTitulo py-4">Garçons</h1>

<div class="row ">
    <div class="col-md-6 d-flex justify-content-center">
        <div class="card mb-4 p-5 boxCliente" style="max-width: 900px;">
            <div class="row g-0">
                <div class="col-md-4">
                    <img src="/assets/img/iconeperfil.png" class="img-fluid rounded-circle" alt="...">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h3 class="card-title">Nome Sobrenome</h3>
                        <p class="card-text">Feminino, 18 anos, Marilia-SP<br>
                            (14)99999-9999</p>
                        <p class="card-text"><small class="text-muted">Cozinheiro</small></p>
                    </div>
                </div>
            </div>
            <h4>Descrição</h4>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sed assumenda aut omnis non excepturi sit dolores ducimus quis quibusdam mollitia deleniti accusantium quos explicabo dolorem, animi voluptatibus veritatis officia in!</p>
            <button class="btn btn-dark" type="submit">Contratar</button>
        </div>
    </div>
    <div class="col-md-6 d-flex justify-content-center">
        <div class="card mb-4 p-5 boxCliente " style="max-width: 900px;">
            <div class="row g-0">
                <div class="col-md-4">
                    <img src="/assets/img/iconeperfil.png" class="img-fluid rounded-circle" alt="...">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h3 class="card-title">Nome Sobrenome</h3>
                        <p class="card-text">Feminino, 18 anos, Marilia-SP<br>
                            (14)99999-9999</p>
                        <p class="card-text"><small class="text-muted">Cozinheiro</small></p>
                    </div>
                </div>
            </div>
            <h4>Descrição</h4>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sed assumenda aut omnis non excepturi sit dolores ducimus quis quibusdam mollitia deleniti accusantium quos explicabo dolorem, animi voluptatibus veritatis officia in!</p>
            <button class="btn btn-dark" type="submit">Contratar</button>
        </div>
    </div>
</div>

<?php include_once RODAPE ?>