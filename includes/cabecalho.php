<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Garmous</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link href="/assets/css/mains.css" rel="stylesheet" type="text/css">

</head>

<header>
    <div class="fundoBackground">
        <div class="container">
            <div class="row">

                <div id="logotipo" class="col-md-2 col-10">
                    <a href="/index.php"><img src="/assets/img/logogarmous.png" alt="logo Garmous" width="100"></a>
                </div>

                <div class="col-2 d-block d-lg-none">
                    <button id="btnMenu" class="btn mt-3">
                        <i class="fas fa-bars"></i>
                    </button>
                </div>

                <nav id="menu" class="col-md-10  ">
                    <div class="d-flex justify-content-end ">
                        <ul>
                            <li><a href="/sobrenos.php" class="botaoFundo">Sobre Nós</a></li>
                            <li><a href="/ajuda.php" class="botaoFundo">Ajuda</a></li>
                            <li><a href="/app.php" class="botaoFundo">App</a></li>
                            <li><a type="/button" class="btn btn-danger" href="/login/login.php">Login</a></li>
                        </ul>
                    </div>

                </nav>
            </div>

        </div>
</header>

<script type="text/javascript" src="/assets/js/main.js"></script>