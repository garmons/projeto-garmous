<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Garmous</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link href="/assets/css/mains.css" rel="stylesheet" type="text/css">
</head>


<footer class="textoBranco fundoBackground py-3">
    <div class="container">
        <div class="row">
            <div class="col-md-3">

                <h4>Institucional</h4>
                <li><a href="index.php" class="textoBranco">Home</a></li>
                <li><a href="sobrenos.php" class="textoBranco">Sobre Nós</a></li>
                <li><a href="ajuda.php" class="textoBranco">Ajuda</a></li>
                <li><a href="app.php" class="textoBranco">App</a></li>


            </div>
            <div class="col-md-3">

                <h4>Categorias</h4>

                <li><a href="garcom.html" class="textoBranco">Garçom</a></li>
                <li><a href="entregador.html" class="textoBranco">Entregador</a></li>
                <li><a href="atendente.html" class="textoBranco">Atendente</a></li>
                <li><a href="barman.html" class="textoBranco">Barman</a></li>
                <li><a href="cozinheiro.html" class="textoBranco">Cozinheiro</a></li>

            </div>

            <div class="col-md-3">

                <h4>Redes Sociais</h4>

                <a href="#"><i class="fa-brands fa-facebook-f textoBranco"></i></a>
                <a href="#"><i class="fa-brands fa-instagram textoBranco"></i></a>
                <a href="#"><i class="fa-brands fa-linkedin textoBranco"></i></a>

            </div>

            <div class="col-md-3">

                <h4>Downloads</h4>
                <a href="#"><img src="/assets/img/googleplay.png" class="img-fluid" alt="Googleplay" width="200px"></a>
                <a href="#"><img src="/assets/img/aplestore.png" class="img-fluid" alt="AppleStore" width="200px"></a>
            </div>
        </div>
    </div>
</footer>


<div id="copyright">
    <p>
        Termos de Uso - Copyright - Politica de Privacidade
    </p>
</div>