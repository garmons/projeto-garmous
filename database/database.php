<?php

function database()
{
    $host= 'localhost';
    $banco= 'garmous';
    $usuario = 'root';
    $senha = '';
    

    try {

        $conn = new PDO("mysql:host=$host;dbname=$banco", $usuario, $senha);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $conn->exec("set names utf8");

        return $conn;

    } catch(PDOException $e) {

        die($e->getMessage());

    }
}